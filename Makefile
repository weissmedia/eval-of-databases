MAKEFLAGS += --warn-undefined-variables --no-print-directory
.SHELLFLAGS := -eu -o pipefail -c

all: help

SHELL:=bash

APP_NAME=eval-of-nosql
JMX_PATH?=$(PWD)/benchmarks
DATA_DIR?=$(PWD)/data

# Enable BuildKit for Docker build
export DOCKER_BUILDKIT:=1
export BUILDKIT_INLINE_CACHE:=1
export COMPOSE_DOCKER_CLI_BUILD:=1

##@ Helpers
help: ## display this help
	@echo "$(APP_NAME)"
	@echo "====================="
	@awk 'BEGIN {FS = ":.*##"; printf "\033[36m\033[0m"} /^[a-zA-Z0-9_%\/-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	@printf "\n"

##@ Run container stack
run/%: DARGS?=--build
run/%: CARGS?=
run/%: REMOVE_ALL?=false
run/%: ## up the stack (e.g. run/pakwheels, run/person or run/ecom)
	@if [[ "$(REMOVE_ALL)" == "true" ]]; then $(MAKE) clean/$(notdir $@); docker-compose -f "docker-compose-$(notdir $@).yml" build; fi
	docker-compose -f "docker-compose-$(notdir $@).yml" $(CARGS) up $(DARGS)

run-jm-report/%: THREADS?=1
run-jm-report/%: LOOPS?=100
run-jm-report/%:  ## run jmeter with report (e.g. make run-jm-report/local-v2 THREADS=2 LOOPS=500)
	$(MAKE) jm/$(notdir $@) jm-report

##@ Remove container
clean/%: ## clean up
	docker-compose -f "docker-compose-$(notdir $@).yml" down --volumes

clean-docker: ## stop and remove container and volumes
	@docker stop $$(docker ps -aq) 2> /dev/null
	@docker rm $$(docker ps -aq) 2> /dev/null
	@docker volume prune -f

##@ Benchmark
benchmark = $(word $2,$(subst -, ,$1))
request_path = $(word $2,$(subst /,-,$1))

jm/%: THREADS?=1
jm/%: RAMP_UP?=1
jm/%: LOOPS?=1
jm/%: LIMIT?=10
jm/%: SAVE_SESSION?=false
jm/%: JVM_ARGS?=-Xms4G -Xmx4G
jm/%: STARTUP_TIME?=20
jm/%: REQUEST_PATH?=/
jm/%: START_DOCKER?=true
jm/%: REMOVE_ALL?=false
jm/%: ## run jmeter benchmark (e.g. make jm/local THREADS=2 LOOPS=500)
	# https://ambertests.com/2018/06/18/using-jmeter-properties-from-the-command-line/
	$(eval BENCHMARK := $(call benchmark,$*,1))
	$(if $(shell [ $(REQUEST_PATH) == "/" ] && echo "OK"), $(eval REQPATH := ""), $(eval REQPATH := $(call request_path, $(REQUEST_PATH) ,1)))
	$(if $(shell [ $(SAVE_SESSION) == "false" ] && echo "OK"), $(eval SAVE_SESSION_INT := 0), $(eval SAVE_SESSION_INT := 1))
	$(if $(shell [ $(REMOVE_ALL) == "false" ] && echo "OK"), $(eval REMOVE_ALL_INT := 0), $(eval REMOVE_ALL_INT := 1))
	@if [[ "$(START_DOCKER)" == "true" ]]; then $(MAKE) run/${BENCHMARK} DARGS="--detach" REMOVE_ALL=$(REMOVE_ALL);sleep $(STARTUP_TIME); fi
	JVM_ARGS="$(JVM_ARGS)" jmeter -J THREAD_GROUP_NAME="Thread Group $(THREADS)-$(SAVE_SESSION_INT)-$(REMOVE_ALL_INT)$(REQPATH)" -J LABEL_SUFFIX="$(THREADS)-$(SAVE_SESSION_INT)-$(REMOVE_ALL_INT)$(REQPATH)" -J THREADS=$(THREADS) -J RAMP_UP=$(RAMP_UP) -J LOOPS=$(LOOPS) -J LIMIT=$(LIMIT) -J REQUEST_PATH="$(REQUEST_PATH)" -n -t $(JMX_PATH)/${BENCHMARK}/$(notdir $@).jmx -l $(DATA_DIR)/${BENCHMARK}/reports/$(notdir $@)-$(THREADS)-$(LOOPS)-$(LIMIT)-$(SAVE_SESSION_INT)-$(REMOVE_ALL_INT)$(REQPATH).csv -f -e -o $(DATA_DIR)/${BENCHMARK}/reports/html

##@ Report
jm-report/%: OUTPUT_DIR?=$(PWD)/data/reports
jm-report/%: DATA_DIR?=$(PWD)/data
jm-report/%: ## open jmeter benchmark report (e.g. make jm-report/local-v5-50-1-100)
	rm -rf $(OUTPUT_DIR)
	jmeter -g $(DATA_DIR)/$(notdir $@) -o $(OUTPUT_DIR)
	open $(DATA_DIR)/reports/index.html
