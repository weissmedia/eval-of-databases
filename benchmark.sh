#!/usr/bin/env bash

benchmark_accesslog () {
   make clean-docker
   ./benchmark-accesslog.sh -rt 20
}

benchmark_pakwheels () {
   make clean-docker
    ./benchmark-pakwheels.sh -r
}

benchmark_twitter () {
   make clean-docker
   ./benchmark-twitter.sh -ri
}

while getopts alpt opt
do
  case $opt in
    a) benchmark_accesslog;benchmark_pakwheels;benchmark_twitter ;;
    l) benchmark_accesslog ;;
    p) benchmark_pakwheels ;;
    t) benchmark_twitter ;;
    *) echo "usage: $0 [-alpt]" >&2
       exit 1 ;;
  esac
done

echo ""
echo "# Generate Graph to PNG with matplot"
echo "# ------------------------------------------------------------------------------"
./plot_graph.py

echo ""
echo "# Generate Aggregate Results to PNG with matplot"
echo "# ------------------------------------------------------------------------------"
./plot_bar.py

echo ""
echo "# Copy all Reports"
echo "# ------------------------------------------------------------------------------"
./copy-reports.py -t ~/Repositories/diplomarbeit-v2/images