import json
import shlex


log_file_name = "data/access-0.log"
jsonl_file_name = "parsed.jsonl"

shlex.split('this is "a test"')


keys = ['host', 'user', 'unknown', 'time', 'request', 'status', 'size', 'referer', 'agent', 'unknown']
with open(log_file_name) as file, open(jsonl_file_name, "w") as jl:
    for line in file:
        lst = shlex.split(line)
        lst[3] = (lst[3]+" "+lst[4])[1:-1]
        del lst[4]
        data = dict(zip(keys, lst))
        del data['unknown']
        jl.write(json.dumps(data))