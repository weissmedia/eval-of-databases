#!/usr/bin/env python
# https://www.grund-wissen.de/informatik/python/scipy/matplotlib.html

import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def main(file):
    *path_lst, filename = file.split('/')
    path = '/'.join(path_lst)
    plot_name = f"{path}/{filename.split('.')[0]}-mp.png"

    diagram_title = None

    df = pd.read_csv(file)
    df.head()

    # Drop last line
    df.drop(df.tail(1).index, inplace=True)

    # Relabelling
    if 'twitter' in file:
        diagram_title = (df['Label'].str.split(' ')).apply(lambda x: ' '.join(x[1].split('-')[3:]).title())[0]
        df['Label'] = (df['Label'].str.split(' ')).apply(lambda x: f"{x[0]} {x[1].split('-')[0]}")
    elif 'accesslog' in file:
        df['Label'] = (df['Label'].str.split(' ')).apply(lambda x: f"{x[0]} {x[1].split('-')[0]}")
    elif 'pakwheels' in file:
        df['Label'] = (df['Label'].str.split(' ')).apply(lambda x: f"{x[0]} {'-'.join(x[1].split('-')[:-1])}")
    else:
        raise Exception()

    df = pd.DataFrame(df, columns=["Label", "Durchschnitt", "Mittel"])
    df.plot(x="Label", y=["Durchschnitt", "Mittel"], kind="bar")

    # Set ticks to int
    min_value = df[["Durchschnitt","Mittel"]].min().min()
    max_value = df[["Durchschnitt","Mittel"]].max().max()
    if max_value < 10:
        plt.yticks(np.arange(min_value, max_value+1, 1.0))

    plt.ylabel('$\it{Milliseconds}$')
    plt.xlabel('')
    plt.legend(['Average', 'Median'])

    if diagram_title:
        plt.title(diagram_title)

    plt.grid(linestyle=':', linewidth='0.8')
    plt.savefig(plot_name,
                dpi=300,
                facecolor='white',
                bbox_inches='tight',
                transparent=False)
    plt.close('all')


if __name__ == '__main__':
    files = glob.glob('./data/*/reports/*_AggregateReport.csv', recursive=False)
    for file in files:
        print(file)
        main(file)

    print('done')
