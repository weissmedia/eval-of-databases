package main

import (
	"bytes"
	"fmt"
	"github.com/caarlos0/env/v6"
	"github.com/go-redis/redis"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
)

type config struct {
	TargetUrl   url.URL `env:"TARGET_URL,required"`
	RedisHost   string  `env:"REDIS_HOST,required"`
	LogLevel    string  `env:"LOG_LEVEL" envDefault:"debug"`
	SaveSession bool    `env:"SAVE_SESSION" envDefault:"false"`
}

func main() {
	cfg := config{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatal(err)
	}
	level, _ := log.ParseLevel(cfg.LogLevel)
	log.SetLevel(level)

	client := redis.NewClient(&redis.Options{Addr: cfg.RedisHost})
	_, err := client.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()

	proxy := http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		log.Debugln(req)

		rw.Header().Set("Content-Type", "application/json")

		u, _ := url.Parse(req.URL.String())
		p, _ := url.ParseQuery(u.RawQuery)

		key := fmt.Sprintf("%v:%v", p["s"][0], p["e"][0])

		if cfg.SaveSession {
			key = fmt.Sprintf("%v:%v", key, p["u"][0])
		}

		result, err := client.Get(key).Bytes()
		if err != redis.Nil {
			_, _ = rw.Write(result)
			return
		} else if err != nil {
			log.Error(err)
		}

		req.Host = cfg.TargetUrl.Host
		req.URL.Host = cfg.TargetUrl.Host
		req.URL.Scheme = cfg.TargetUrl.Scheme
		req.RequestURI = ""

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			_, _ = fmt.Fprint(rw, err)
			return
		}

		bodyBytes, _ := io.ReadAll(resp.Body)

		if err := client.Set(key, bodyBytes, 0).Err(); err != nil {
			log.Fatal(err)
		}

		fmt.Println("New record is:", key)
		resp.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		rw.WriteHeader(http.StatusOK)
		_, _ = io.Copy(rw, resp.Body)
	})

	log.Fatal(http.ListenAndServe(":80", proxy))
}
