module pakwheels-redis

go 1.18

require (
	github.com/caarlos0/env/v6 v6.9.3
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/lib/pq v1.10.6
	github.com/sirupsen/logrus v1.8.1
)

require (
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
)
