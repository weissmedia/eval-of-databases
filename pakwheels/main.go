package main

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strconv"
)

type KaggleData struct {
	UsedCars []struct {
		Type  string `json:"@type"`
		Brand struct {
			Type string `json:"@type"`
			Name string `json:"name"`
		} `json:"brand"`
		Model               string `json:"model"`
		Description         string `json:"description"`
		ItemCondition       string `json:"itemCondition"`
		ModelDate           int    `json:"modelDate"`
		Manufacturer        string `json:"manufacturer"`
		FuelType            string `json:"fuelType"`
		Name                string `json:"name"`
		Image               string `json:"image"`
		VehicleTransmission string `json:"vehicleTransmission"`
		Color               string `json:"color"`
		BodyType            string `json:"bodyType"`
		VehicleEngine       struct {
			Type               string `json:"@type"`
			EngineDisplacement string `json:"engineDisplacement"`
		} `json:"vehicleEngine"`
		MileageFromOdometer string   `json:"mileageFromOdometer"`
		SellerLocation      string   `json:"sellerLocation"`
		PostedFrom          string   `json:"postedFrom"`
		Keywords            string   `json:"keywords"`
		Features            []string `json:"features"`
		AdLastUpdated       string   `json:"adLastUpdated"`
		Price               int      `json:"price"`
		PriceCurrency       string   `json:"priceCurrency"`
	} `json:"usedCars"`
}

func init() {
	log.SetLevel(log.DebugLevel)
}

func main() {
	data, err := ioutil.ReadFile("usedCars.json")
	if err != nil {
		log.Fatal(err)
	}

	kd := KaggleData{}
	if err = json.Unmarshal(data, &kd); err != nil {
		log.Fatal(err)
	}

	start, end := 0, len(kd.UsedCars)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()
		if q.Has("s") {
			start, _ = strconv.Atoi(q.Get("s"))
		}
		if q.Has("e") {
			end, _ = strconv.Atoi(q.Get("e"))
		}
		log.Println(start, end)
		w.Header().Set("Content-Type", "application/json")
		err := json.NewEncoder(w).Encode(kd.UsedCars[start:end])
		if err != nil {
			log.Fatal(err)
			return
		}
	})
	log.Fatal(http.ListenAndServe(":80", nil))
}
