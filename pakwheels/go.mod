module cars-api

go 1.18

require github.com/sirupsen/logrus v1.8.1

require (
	github.com/caarlos0/env/v6 v6.9.2 // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
)
