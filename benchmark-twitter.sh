#!/usr/bin/env bash

BENCHMARK=twitter
VERSION=v1
BENCHMARK_VERSION="${BENCHMARK}-${VERSION}"
BENCHMARK_REPORTS_PATH=./data/${BENCHMARK}/reports
RAMP_UP=1
LOOPS=10
LIMIT=1
STARTUP_TIME=30
START_DOCKER=true
SAVE_SESSION=false
SAVE_SESSION_INT=$([ $SAVE_SESSION == "false" ] && echo "0" || echo "1")
REMOVE_ALL=false
REMOVE_ALL_INT=$([ $REMOVE_ALL == "false" ] && echo "0" || echo "1")

while getopts sirt:l: opt
do
  case $opt in
    r) rm -rf "${BENCHMARK_REPORTS_PATH}";;
    s) START_DOCKER=false;STARTUP_TIME=0;;
    i) ./twitter-init.sh -d; START_DOCKER=false;STARTUP_TIME=0;;
    t) STARTUP_TIME=$OPTARG;;
    l) LOOPS=$OPTARG;;
    *) echo "usage: $0 [-r]" >&2
       exit 1 ;;
  esac
done

mkdir -p "${BENCHMARK_REPORTS_PATH}"

requests=(/your-mentions /most-influential-followers /most-tagged /follower-recommendations /common-hashtags)
threads=(1 2 5 10 20 30 50)
for REQUEST_PATH in "${requests[@]}"; do
  REQ_PATH=$(echo "${REQUEST_PATH}" | sed 's/\//-/g')
  resultsTemp="${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${LOOPS}-${LIMIT}${REQ_PATH}.tmp"
  rm "${resultsTemp}" 2> /dev/null
  for THREADS in "${threads[@]}"; do
    echo ""
    echo "::GROUP:: THREADS=${THREADS} LOOPS=${LOOPS} RAMP_UP=${RAMP_UP} LIMIT=${LIMIT} START_DOCKER=${START_DOCKER} STARTUP_TIME=${STARTUP_TIME} REQUEST_PATH=${REQUEST_PATH}"
    echo "# ------------------------------------------------------------------------------"
    make jm/${BENCHMARK_VERSION} START_DOCKER="${START_DOCKER}" THREADS="${THREADS}" RAMP_UP="${RAMP_UP}" LIMIT="${LIMIT}" LOOPS="${LOOPS}" STARTUP_TIME="${STARTUP_TIME}" REQUEST_PATH="${REQUEST_PATH}"

    echo ""
    echo "# Merge Results to ${resultsTemp}"
    echo "# ------------------------------------------------------------------------------"
    cat <(tail +2 "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}-${SAVE_SESSION_INT}-${REMOVE_ALL_INT}${REQ_PATH}.csv") >> "${resultsTemp}"

    echo ""
    echo "# Generate Graph to PNG for THREADS=${THREADS} LOOPS=${LOOPS} LIMIT=${LIMIT} REQUEST_PATH=${REQUEST_PATH}"
    echo "# ------------------------------------------------------------------------------"
    ./generate-graph2png.sh "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}-${SAVE_SESSION_INT}-${REMOVE_ALL_INT}${REQ_PATH}.csv"
    echo "::ENDGROUP::"
  done

  results=".$(echo "${resultsTemp}" | cut -d"." -f2).csv"
  cat <(head -n 1 "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}-${SAVE_SESSION_INT}-${REMOVE_ALL_INT}${REQ_PATH}.csv") <(tail +2 "${resultsTemp}") > "${results}"

  echo ""
  echo "# Generate Graph to PNG"
  echo "# ------------------------------------------------------------------------------"
  ./generate-graph2png.sh "${results}"

  echo ""
  echo "# Generate Aggregate Results CSV from ${results}"
  echo "# ------------------------------------------------------------------------------"
  ./aggregate-results.sh "${results}"

done

echo ""
echo "# Stop and remove container ${BENCHMARK}"
echo "# ------------------------------------------------------------------------------"
docker stop $(docker ps -aq) 2> /dev/null
docker rm $(docker ps -aq) 2> /dev/null
