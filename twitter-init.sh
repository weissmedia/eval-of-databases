#!/bin/bash

DOCKER_ARGS=""
STARTUP_TIME=0

while getopts d opt
do
  case $opt in
    d) DOCKER_ARGS="-d";STARTUP_TIME=60;;
    *) echo "usage: $0 [-r]" >&2
       exit 1 ;;
  esac
done

docker stop $(docker ps -aq) 2> /dev/null
docker rm $(docker ps -aq) 2> /dev/null
docker volume prune -f
make run/twitter DARGS="--build -d twitter-neo4j"
docker exec -it eval-of-nosql_twitter-neo4j_1 neo4j-admin load --from /import/twitter-v2-43.dump --database "twitter"
docker stop $(docker ps -aq)
make run/twitter DARGS=${DOCKER_ARGS} CARGS="--env-file .envs/.twitter"
sleep "${STARTUP_TIME}"
