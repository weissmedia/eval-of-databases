#!/usr/bin/awk -f
#
# Programname: datetimecounts.awk

#awk 'BEGIN{print "datatime,count"}{a[substr($4,2,20)]+=1}END{for(b in a){print b,a[b]}}' data/accesslog/access-log-online-shopping-store-zanbil/access.log | sort -nk2 > data/accesslog/access-log-online-shopping-store-zanbil/access-datetime-counts.log

BEGIN {
    print "datatime,count"
}
{
    a[substr($4,2,20)]+=1
}
END {
    for(b in a){print b,a[b]}
}