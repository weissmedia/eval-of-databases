#!/usr/bin/env bash

BENCHMARK=pakwheels
VERSION=v5
BENCHMARK_VERSION="${BENCHMARK}-${VERSION}"
BENCHMARK_REPORTS_PATH=./data/${BENCHMARK}/reports
LOOPS=10
LIMIT=100

while getopts r opt
do
  case $opt in
    r) rm -rf "${BENCHMARK_REPORTS_PATH}";;
    *) echo "usage: $0 [-r]" >&2
       exit 1 ;;
  esac
done

mkdir -p "${BENCHMARK_REPORTS_PATH}"

resultsTemp="${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${LOOPS}-${LIMIT}.tmp"
rm "${resultsTemp}" 2> /dev/null

threads=(5 50 60 70 80)
for THREADS in "${threads[@]}"; do
  echo ""
  echo "# Clean up ${BENCHMARK}"
  echo "# ------------------------------------------------------------------------------"
  make clean/${BENCHMARK}

  # Alle Anfragen (Requests) müssen berechnet werden.
  echo ""
  echo "# Group 1-1"
  echo "# ------------------------------------------------------------------------------"
  make jm/${BENCHMARK_VERSION} THREADS="${THREADS}" LIMIT="${LIMIT}" LOOPS="${LOOPS}" SAVE_SESSION=true REMOVE_ALL=true

  # Der erste Anfrage (Request) muss berechnet werden.
  # Alle weiteren Anfragen (Requests) werden aus dem Cache bedient.
  echo ""
  echo "# Group 0-1"
  echo "# ------------------------------------------------------------------------------"
  make jm/${BENCHMARK_VERSION} THREADS="${THREADS}" LIMIT="${LIMIT}" LOOPS="${LOOPS}" SAVE_SESSION=false REMOVE_ALL=true

  echo ""
  echo "# Merge Results to ${resultsTemp}"
  echo "# ------------------------------------------------------------------------------"
  cat "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}-0-1.csv" <(tail +2 "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}-1-1.csv") > "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}.csv"
  cat <(tail +2 "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}.csv") >> "${resultsTemp}"

  echo ""
  echo "# Generate Graph to PNG for THREADS=${THREADS} LOOPS=${LOOPS} LIMIT=${LIMIT}"
  echo "# ------------------------------------------------------------------------------"
  ./generate-graph2png.sh "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}.csv"
done

results=".$(echo "${resultsTemp}" | cut -d"." -f2).csv"
cat <(head -n 1 "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}.csv") <(tail +2 ${resultsTemp}) > "${results}"

echo ""
echo "# Generate Graph to PNG"
echo "# ------------------------------------------------------------------------------"
./generate-graph2png.sh "${results}"

echo ""
echo "# Generate Aggregate Results CSV from ${results}"
echo "# ------------------------------------------------------------------------------"
./aggregate-results.sh "${results}"

echo ""
echo "# Stop and remove container ${BENCHMARK}"
echo "# ------------------------------------------------------------------------------"
docker stop $(docker ps -aq) 2> /dev/null
docker rm $(docker ps -aq) 2> /dev/null