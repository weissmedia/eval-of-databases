#!/usr/bin/env python
# https://www.grund-wissen.de/informatik/python/scipy/matplotlib.html

import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline


def main(file):
    *path_lst, filename = file.split('/')
    path = '/'.join(path_lst)
    graphic_name = f"{path}/{filename.split('.')[0]}_ResponseTimesPercentiles-mp.png"

    diagram_title = None

    X = pd.read_csv(file)
    X.head()

    elapsed = pd.DataFrame(columns=X.label.unique())
    quantiles = np.arange(0, 0.99, 0.01)

    for col in elapsed.columns:
        elapsed[col] = X.elapsed[X.label == col].quantile(quantiles)

    elapsed.set_index(keys=quantiles*100, inplace=True)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xticks(range(0, 110, 10))

    column_size = len(elapsed.columns)

    Y_max = 0

    for i, col in enumerate(elapsed.columns):
        diagram_title = None
        c = col
        if 'twitter' in file:
            diagram_title = ' '.join(c.split('-')[3:]).title()
            c = c.split('-')[0]
        elif 'accesslog' in file:
            c = c.replace('-0-0', '')
        elif 'pakwheels' in file:
            c = c[:-2]

        X_Y_Spline = make_interp_spline(quantiles, elapsed[col])
        X_ = np.linspace(quantiles.min(), quantiles.max(), 20)
        Y_ = X_Y_Spline(X_)
        if Y_max < Y_.max():
            Y_max = Y_.max()


        lines = ax.plot(X_*100, Y_, label=c)
        if 'postgre' in c.lower():
            lines[0].set_linestyle('solid')
        else:
            lines[0].set_linestyle('dashed')

    #step = round(Y_max/7)
    #print(step)
    #plt.yticks(np.arange(0, round(Y_max)+step, step))
    plt.grid(linestyle=':', linewidth='0.8')
    plt.ylabel('$\it{Percentile}$ $\it{value}$ $\it{in}$ $\it{ms}$')
    plt.xlabel('$\it{Percentiles}$')
    plt.legend(bbox_to_anchor=(0.5, -0.2), loc='upper center', ncol=2 if column_size <= 4 else 3)

    if diagram_title:
        plt.title(diagram_title)

    plt.savefig(graphic_name,
                dpi=300,
                facecolor='white',
                bbox_inches='tight',
                transparent=False)
    plt.close('all')


if __name__ == '__main__':
    files = glob.glob('./data/*/reports/*.csv', recursive=False)
    skip = ('AggregateReport',)
    for file in files:
        if any(i in file for i in skip):
            print(f"skip: {file}")
            continue
        print(file)
        main(file)

    print('done')
