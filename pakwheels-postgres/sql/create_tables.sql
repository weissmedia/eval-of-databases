CREATE SEQUENCE cache_id_seq;
CREATE TABLE IF NOT EXISTS cache_item (
    id integer NOT NULL DEFAULT nextval('cache_id_seq'),
    key varchar NOT NULL,
    data bytea NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS cache (
    key varchar NOT NULL,
    data bytea NOT NULL,
    PRIMARY KEY (key)
);