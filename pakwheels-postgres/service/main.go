package main

import (
	"bytes"
	"database/sql"
	"fmt"
	"github.com/caarlos0/env/v6"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
)

type config struct {
	TargetUrl   url.URL `env:"TARGET_URL,required"`
	PostgresUrl string  `env:"POSTGRES_URL,required"`
	LogLevel    string  `env:"LOG_LEVEL" envDefault:"debug"`
	SaveSession bool    `env:"SAVE_SESSION" envDefault:"false"`
}

func main() {
	cfg := config{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatal(err)
	}
	level, _ := log.ParseLevel(cfg.LogLevel)
	log.SetLevel(level)

	db, err := sql.Open("postgres", cfg.PostgresUrl)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	proxy := http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		log.Infoln(req)

		rw.Header().Set("Content-Type", "application/json")

		u, _ := url.Parse(req.URL.String())
		p, _ := url.ParseQuery(u.RawQuery)

		key := fmt.Sprintf("%v:%v", p["s"][0], p["e"][0])

		if cfg.SaveSession {
			key = fmt.Sprintf("%v:%v", key, p["u"][0])
		}

		result := []byte("")
		_ = db.QueryRow("SELECT data FROM cache WHERE key=$1;", key).Scan(&result)

		log.Infoln("Result length: ", len(result))

		if len(result) > 0 {
			_, _ = rw.Write(result)
			return
		}

		req.Host = cfg.TargetUrl.Host
		req.URL.Host = cfg.TargetUrl.Host
		req.URL.Scheme = cfg.TargetUrl.Scheme
		req.RequestURI = ""

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			_, _ = fmt.Fprint(rw, err)
			return
		}

		bodyBytes, _ := io.ReadAll(resp.Body)

		sqlStatement := `INSERT INTO cache (key, data) VALUES ($1, $2) ON CONFLICT (key) DO NOTHING RETURNING key;`
		retKey := ""
		if dbErr := db.QueryRow(sqlStatement, key, bodyBytes).Scan(&retKey); dbErr != nil {
			fmt.Println("Duplicate key:", key)
		} else {
			fmt.Println("New record is:", retKey)
		}

		resp.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		rw.WriteHeader(http.StatusOK)
		_, _ = io.Copy(rw, resp.Body)
	})

	log.Fatal(http.ListenAndServe(":80", proxy))
}
