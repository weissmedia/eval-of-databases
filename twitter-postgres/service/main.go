package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/caarlos0/env/v6"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type config struct {
	PostgresUrl string `env:"POSTGRES_URL,required"`
	LogLevel    string `env:"LOG_LEVEL" envDefault:"debug"`
}

func getResults(db *sql.DB, query string) interface{} {
	rows, err := db.Query(query)
	if err != nil {
		fmt.Println(err.Error())
	}
	columns, err := rows.Columns()
	if err != nil {
		log.Fatal(err.Error())
	}
	values := make([]interface{}, len(columns))
	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}
	var results []map[string]interface{}
	for rows.Next() {
		item := make(map[string]interface{})
		err = rows.Scan(scanArgs...)
		if err != nil {
			log.Fatal(err.Error())
		}
		for i, value := range values {
			switch value.(type) {
			case nil:
				item[columns[i]] = nil
			case []byte:
				item[columns[i]] = string(value.([]byte))
			default:
				item[columns[i]] = value
			}
		}
		results = append(results, item)
	}
	return results
}

func main() {
	cfg := config{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatal(err)
	}
	level, _ := log.ParseLevel(cfg.LogLevel)
	log.SetLevel(level)

	db, err := sql.Open("postgres", cfg.PostgresUrl)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
	})
	http.HandleFunc("/your-mentions", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		query := `SELECT u.screen_name,  COUNT(u.screen_name) count
					FROM me_user m
					JOIN posts rp ON rp._start = m._id
					JOIN tweet t ON t._id = rp."_end"
					JOIN mentions rm ON rm._start = t._id
					JOIN "user" u ON u._id = rm._end
					GROUP BY u.screen_name
					ORDER BY COUNT(u.screen_name) DESC
					LIMIT 10;`

		err = json.NewEncoder(w).Encode(getResults(db, query))
		if err != nil {
			log.Println("error response:", err)
		}
	})
	http.HandleFunc("/most-influential-followers", func(w http.ResponseWriter, r *http.Request) {
		query := `SELECT
					u.screen_name,
					u.followers 
					FROM
					"user" u
					JOIN follows rf ON rf."_start" = u."_id" 
					ORDER BY
					u.followers::INT DESC 
					LIMIT 10;`
		w.Header().Set("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(getResults(db, query))
		if err != nil {
			log.Println("error response:", err)
		}
	})
	http.HandleFunc("/most-tagged", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		query := `SELECT h.name, COUNT(h) Hashtags
					FROM hashtag h
					JOIN tags rt ON rt._end = h._id
					JOIN tweet t ON t._id = rt."_start"
					JOIN posts rp ON rp._end = t._id
					JOIN "me_user" m ON m._id = rp._start
					GROUP BY h.name
					ORDER BY COUNT(h) DESC
					LIMIT 10;`

		err = json.NewEncoder(w).Encode(getResults(db, query))
		if err != nil {
			log.Println("error response:", err)
		}
	})
	http.HandleFunc("/follower-recommendations", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		query := `SELECT ou.screenname 
					FROM (
  						SELECT DISTINCT u.screen_name AS screenname, u.*, m.*
  						FROM "user" u
           				JOIN posts rp ON rp._start = u._id
						JOIN tweet t ON t._id = rp."_end"
						JOIN mentions rm ON rm._start = t._id
						JOIN "me_user" m ON m._id = rm._end
  						WHERE u._id  IN (
							SELECT _id 
							FROM "user" u
							JOIN follows rf ON rf._start = u._id) AND u._id NOT IN (select _id from "user" u
							JOIN follows rf ON rf._end = u._id
						)
					) ou
					ORDER BY UPPER(ou.screenname);`

		err = json.NewEncoder(w).Encode(getResults(db, query))
		if err != nil {
			log.Println("error response:", err)
		}
	})
	http.HandleFunc("/common-hashtags", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		query := `SELECT sugg.friend, ARRAY_LENGTH(ARRAY_AGG(DISTINCT sugg.tags),1) AS common
					FROM (
						SELECT *, u.screen_name AS friend, u._id AS uid, h._id AS hid, t._id AS tid, h.name AS tags
							FROM "user" u
							JOIN posts rp ON rp._start = u._id
							JOIN tweet t ON t._id = rp."_end"
							JOIN tags rt ON rt._start = t._id
							JOIN "hashtag" h ON h._id = rt._end
						) sugg
						JOIN (
							SELECT *,m._id AS mid, h._id AS hid, t._id AS tid, h.name AS tags
							FROM me_user m
							JOIN posts rp ON rp._start = m._id
							JOIN tweet t ON t._id = rp."_end"
							JOIN tags rt ON rt._start = t._id
							JOIN "hashtag" h ON h._id = rt._end
						) me ON sugg.hid = me.hid AND sugg.uid <> me.mid
					GROUP BY sugg.friend
					ORDER BY common DESC
					LIMIT 20;`

		err = json.NewEncoder(w).Encode(getResults(db, query))
		if err != nil {
			log.Println("error response:", err)
		}
	})
	log.Fatal(http.ListenAndServe(":80", nil))
}
