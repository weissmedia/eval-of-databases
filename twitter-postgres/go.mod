module twitter-postgres

go 1.18

require (
	github.com/caarlos0/env/v6 v6.9.3
	github.com/lib/pq v1.10.6
	github.com/sirupsen/logrus v1.8.1
)

require golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
