--https://guides.neo4j.com/sandbox/twitter/index.html
/*
Common Hashtags

What users tweet with some of your top hashtags?

Click on the code block
Notice it gets copied to the editor above ↑
Click the editor's play button to execute
Wait for the query to finish
// Users tweeting with your top hashtags
MATCH
  (me:User:Me)-[:POSTS]->(tweet:Tweet)-[:TAGS]->(ht)
MATCH
  (ht)<-[:TAGS]-(tweet2:Tweet)<-[:POSTS]-(sugg:User)
WHERE
  sugg <> me
WITH
  sugg, collect(distinct(ht)) as tags
RETURN
  sugg.screen_name as friend, size(tags) as common
ORDER BY
  common DESC
LIMIT 20
*/



select sugg.friend, ARRAY_LENGTH(ARRAY_AGG(DISTINCT sugg.tags),1) as common
from (
         select *, u.screen_name as friend, u._id as uid, h._id as hid, t._id as tid, h.name as tags
         from "user" u
                  JOIN posts rp on rp._start = u._id
             JOIN tweet t on t._id = rp."_end"
             JOIN tags rt on rt._start = t._id
             JOIN "hashtag" h on h._id = rt._end) sugg
         join
     (select *,m._id as mid, h._id as hid, t._id as tid, h.name as tags
      from me_user m
               JOIN posts rp on rp._start = m._id
          JOIN tweet t on t._id = rp."_end"
          JOIN tags rt on rt._start = t._id
          JOIN "hashtag" h on h._id = rt._end) me on sugg.hid = me.hid and sugg.uid <> me.mid
GROUP BY sugg.friend
ORDER BY common desc
    limit 20;

/*
Follower Recommendations

Who tweets about you, but you do not follow?

Click on the code block
Notice it gets copied to the editor above ↑
Click the editor's play button to execute
Wait for the query to finish
// Follower Recommendations - tweeting about you, but you don't follow
MATCH
  (ou:User)-[:POSTS]->(t:Tweet)-[mt:MENTIONS]->(me:User:Me)
WITH
  DISTINCT ou, me
WHERE
  (ou)-[:FOLLOWS]->(me)
  AND NOT
    (me)-[:FOLLOWS]->(ou)
RETURN
  ou.screen_name
order by
  toUpper(ou.screen_name)
*/

select ou.screenname from (
  select DISTINCT u.screen_name as screenname, u.*, m.*
  from "user" u
           JOIN posts rp on rp._start = u._id
      JOIN tweet t on t._id = rp."_end"
      JOIN mentions rm on rm._start = t._id
      JOIN "me_user" m on m._id = rm._end
  WHERE u._id  in (select _id from "user" u
      join follows rf on rf._start = u._id) and u._id not in (select _id from "user" u
      join follows rf on rf._end = u._id)
) ou
ORDER BY
    upper(ou.screenname);

/*Version nur mit :User und ohne :Me:User*/
SELECT
 ou.screenname
FROM (
 SELECT
  DISTINCT u.screen_name AS screenname, u.*, m.*
 FROM
  "user" u
  JOIN posts rp ON rp._start = u._id
  JOIN tweet T ON T._id = rp."_end"
  JOIN mentions rm ON rm._start = T._id
  JOIN ( SELECT * FROM "user" u WHERE u._labels = ':Me:User' ) M ON M._id = rm._end
  WHERE
   u._id IN ( SELECT _id FROM "user" u JOIN follows rf ON rf._start = u._id )
   AND u._id NOT IN ( SELECT _id FROM "user" u JOIN follows rf ON rf._end = u._id )
) ou
ORDER BY UPPER ( ou.screenname );
/*
Most Tagged

What hashtags have you used most often?

Click on the code block
Notice it gets copied to the editor above ↑
Click the editor's play button to execute
Wait for the query to finish
// The hashtags you have used most often

MATCH
  (h:Hashtag)<-[:TAGS]-(t:Tweet)<-[:POSTS]-(u:User:Me)
WITH
  h, COUNT(h) AS Hashtags
ORDER BY
  Hashtags DESC
LIMIT 10
RETURN
  h.name, Hashtags
*/

select u.name, COUNT(u) Hashtags
from me_user m
         JOIN posts rp on rp._start = m._id
    JOIN tweet t on t._id = rp."_end"
    JOIN tags rm on rm._start = t._id
    JOIN "hashtag" u on u._id = rm._end
GROUP BY u.name
ORDER BY COUNT(u) desc
limit 10;

select h.name, COUNT(h) Hashtags
from hashtag h
         JOIN tags rt on rt._end = h._id
    JOIN tweet t on t._id = rt."_start"
    JOIN posts rp on rp._end = t._id
JOIN "me_user" m on m._id = rp._start
GROUP BY h.name
ORDER BY COUNT(h) desc
limit 10;

/*
Your mentions

To the right is a giant code block containing a single Cypher query statement to determine who's mentioning you on Twitter.

Click on the code blocks
Notice they get copied to the editor above ↑
Click the editor's play button to execute
Wait for the query to finish
Graph of some of your mentions

// Graph of some of your mentions
MATCH
  (u:Me:User)-[p:POSTS]->(t:Tweet)-[:MENTIONS]->(m:User)
WITH
  u,p,t,m, COUNT(m.screen_name) AS count
ORDER BY
  count DESC
RETURN
  m.screen_name, COUNT(m.screen_name)
ORDER BY
    COUNT(m.screen_name) DESC
LIMIT 10
*/
select u.screen_name,  COUNT(u.screen_name) count
from me_user m
    JOIN posts rp on rp._start = m._id
    JOIN tweet t on t._id = rp."_end"
    JOIN mentions rm on rm._start = t._id
    JOIN "user" u on u._id = rm._end
--WHERE u.screen_name = 'jimwebber'
GROUP BY u.screen_name
ORDER BY count(u.screen_name) desc
limit 10

--select u.screen_name,  COUNT(u.screen_name) count from me_user m
--JOIN node_rel r on r._start = m._id and r._type = 'POSTS'
--JOIN tweet t on t._id = r._end
--JOIN node_rel r2 on r2._start = t._id and r2._type = 'MENTIONS'
--JOIN "user" u on u._id = r2._end
--GROUP BY u.screen_name
--ORDER BY count(u.screen_name) desc
;

/*
Who are your most influential followers?

Click on the code block
Notice it gets copied to the editor above ↑
Click the editor's play button to execute
Wait for the query to finish
// Most influential followers
MATCH
  (follower:User)-[:FOLLOWS]->(u:User:Me)
RETURN
  follower.screen_name AS user, follower.followers AS followers
ORDER BY
  followers DESC
LIMIT 10
 */