/*id,screenName,tags,avatar,followersCount,friendsCount,lang,lastSeen,tweetId,friends*/
CREATE TABLE IF NOT EXISTS "user" (
    id BIGINT NOT NULL,
    screenName varchar NOT NULL,
    tags text[] NOT NULL,
    avatar varchar NOT NULL,
    followersCount integer NOT NULL,
    friendsCount integer NOT NULL,
    lang varchar NOT NULL,
    lastSeen BIGINT NOT NULL,
    tweetId BIGINT NOT NULL,
    friends BIGINT[] NOT NULL
);

COPY "user" (id, screenName, tags, avatar, followersCount, friendsCount, lang, lastSeen, tweetId, friends)
FROM '/data.csv'
DELIMITER ';'
CSV HEADER;

CREATE TABLE friendship (
    user_id BIGINT NOT NULL,
    follower_id BIGINT NOT NULL
);

insert into friendship
    select tu.id as user_id, unnest(tu.friends) follower_id from "user" tu;

ALTER TABLE "user"
    DROP COLUMN followersCount,
    DROP COLUMN friendsCount,
    DROP COLUMN lang,
    DROP COLUMN lastSeen,
    DROP COLUMN tweetId,
    DROP COLUMN friends;

CREATE INDEX idx_tags on "user" USING GIN (tags);
CREATE INDEX idx_follower_id ON friendship (follower_id);
ALTER TABLE "user" ADD PRIMARY KEY (id);
ALTER TABLE friendship ADD COLUMN id BIGSERIAL PRIMARY KEY;
ALTER TABLE friendship ADD CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES "user" (id);

ALTER TABLE friendship ADD COLUMN deletion BIGINT DEFAULT 1;

UPDATE friendship
SET deletion=0
FROM "user"
WHERE "user"."id" = friendship.follower_id;

DELETE FROM friendship WHERE deletion=1;

ALTER TABLE friendship
    DROP deletion,
    DROP id,
    ADD CONSTRAINT fk_user_follower FOREIGN KEY(follower_id) REFERENCES "user" (id);
