package main

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"github.com/caarlos0/env/v6"
	log "github.com/sirupsen/logrus"
	// https://github.com/mongodb/mongo-go-driver/tree/master/examples/documentation_examples
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"net/http"
	"os"
	"strconv"
)

type config struct {
	MongoDBUri        string `env:"MONGODB_URI,required"`
	DefaultDB         string `env:"DEFAULT_DB" envDefault:"mongodb"`
	DefaultCollection string `env:"DEFAULT_COLLECTION" envDefault:"accesslog"`
	LogLevel          string `env:"LOG_LEVEL" envDefault:"debug"`
	DataFile          string `env:"DATA_FILE" envDefault:"/data.jsonl"`
}

type AccessLog struct {
	Host    string `json:"host"`
	User    string `json:"user"`
	Time    string `json:"time"`
	Request string `json:"request"`
	Status  string `json:"status"`
	Size    string `json:"size"`
	Referer string `json:"referer"`
	Agent   string `json:"agent"`
}

func main() {
	cfg := config{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatal(err)
	}

	level, _ := log.ParseLevel(cfg.LogLevel)
	log.SetLevel(level)

	opts := options.Client().ApplyURI(cfg.MongoDBUri)
	client, err := mongo.Connect(context.TODO(), opts)
	if err != nil {
		log.Fatal(err)
	}
	if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
		log.Fatal(err)
	}
	db := client.Database(cfg.DefaultDB)
	coll := db.Collection(cfg.DefaultCollection)

	start, end := 0, 0
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()
		if q.Has("s") {
			start, _ = strconv.Atoi(q.Get("s"))
		}
		if q.Has("e") {
			end, _ = strconv.Atoi(q.Get("e"))
		}
		log.Println(start, end)
		w.Header().Set("Content-Type", "application/json")

		dataFile, err := os.Open(cfg.DataFile)
		if err != nil {
			log.Fatal(err)
		}
		defer dataFile.Close()
		scanner := bufio.NewScanner(dataFile)

		i := 0
		for scanner.Scan() {
			i++
			if i < start {
				continue
			}
			accessLog := AccessLog{}
			if err = json.Unmarshal(scanner.Bytes(), &accessLog); err != nil {
				log.Fatal(err)
			}
			result, err := coll.InsertOne(context.TODO(), accessLog)
			if err != nil {
				http.Error(w, err.Error(), http.StatusServiceUnavailable)
			}
			id := result.InsertedID
			res := fmt.Sprintf("Inserted document (%v) with _id: %v", i, id)
			log.Println(res)
			fmt.Fprintln(w, res)
			if i >= end {
				break
			}
		}
	})
	log.Fatal(http.ListenAndServe(":80", nil))
}
