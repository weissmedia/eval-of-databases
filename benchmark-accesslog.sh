#!/usr/bin/env bash

BENCHMARK=accesslog
VERSION=v3
BENCHMARK_VERSION="${BENCHMARK}-${VERSION}"
BENCHMARK_REPORTS_PATH=./data/${BENCHMARK}/reports
RAMP_UP=1
LOOPS=10
LIMIT=1
STARTUP_TIME=10
SAVE_SESSION=false
SAVE_SESSION_INT=$([ $SAVE_SESSION == "false" ] && echo "0" || echo "1")
REMOVE_ALL=false
REMOVE_ALL_INT=$([ $REMOVE_ALL == "false" ] && echo "0" || echo "1")

while getopts rt:l: opt
do
  case $opt in
    r) rm -rf "${BENCHMARK_REPORTS_PATH}";;
    t) STARTUP_TIME=$OPTARG;;
    l) LOOPS=$OPTARG;;
    *) echo "usage: $0 [-r]" >&2
       exit 1 ;;
  esac
done

mkdir -p "${BENCHMARK_REPORTS_PATH}"

resultsTemp="${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${LOOPS}-${LIMIT}.tmp"
rm "${resultsTemp}" 2> /dev/null

threads=(5 20 50 70 80 90 100 200 300 500)
for THREADS in "${threads[@]}"; do
  echo ""
  echo "::GROUP:: THREADS=${THREADS} LOOPS=${LOOPS} RAMP_UP=${RAMP_UP} LIMIT=${LIMIT} STARTUP_TIME=${STARTUP_TIME}"
  echo "# ------------------------------------------------------------------------------"
  make jm/${BENCHMARK_VERSION} THREADS="${THREADS}" RAMP_UP="${RAMP_UP}" LIMIT="${LIMIT}" LOOPS="${LOOPS}" STARTUP_TIME="${STARTUP_TIME}"

  echo ""
  echo "# Merge Results to ${resultsTemp}"
  echo "# ------------------------------------------------------------------------------"
  cat <(tail +2 "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}-${SAVE_SESSION_INT}-${REMOVE_ALL_INT}${REQ_PATH}.csv") >> "${resultsTemp}"

  echo ""
  echo "# Generate Graph to PNG for THREADS=${THREADS} LOOPS=${LOOPS} LIMIT=${LIMIT}"
  echo "# ------------------------------------------------------------------------------"
  ./generate-graph2png.sh "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}-${SAVE_SESSION_INT}-${REMOVE_ALL_INT}${REQ_PATH}.csv"
  echo "::ENDGROUP::"
done

results=".$(echo "${resultsTemp}" | cut -d"." -f2).csv"
cat <(head -n 1 "${BENCHMARK_REPORTS_PATH}/${BENCHMARK_VERSION}-${THREADS}-${LOOPS}-${LIMIT}-${SAVE_SESSION_INT}-${REMOVE_ALL_INT}${REQ_PATH}.csv") <(tail +2 "${resultsTemp}") > "${results}"

echo ""
echo "# Generate Graph to PNG"
echo "# ------------------------------------------------------------------------------"
./generate-graph2png.sh "${results}"

echo ""
echo "# Generate Aggregate Results CSV from ${results}"
echo "# ------------------------------------------------------------------------------"
./aggregate-results.sh "${results}"

echo ""
echo "# Stop and remove container ${BENCHMARK}"
echo "# ------------------------------------------------------------------------------"
docker stop $(docker ps -aq) 2> /dev/null
docker rm $(docker ps -aq) 2> /dev/null
