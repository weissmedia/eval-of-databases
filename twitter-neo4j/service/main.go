package main

import (
	"encoding/json"
	"fmt"
	"github.com/caarlos0/env/v6"
	_ "github.com/lib/pq"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type config struct {
	Neo4jUri string `env:"NEO4J_URI,required"`
	Database string `env:"DEFAULT_DB,required"`
	LogLevel string `env:"LOG_LEVEL" envDefault:"debug"`
}

func getResults(session neo4j.Session, query string) interface{} {
	results, err := session.ReadTransaction(func(tx neo4j.Transaction) (interface{}, error) {
		records, err := tx.Run(query, nil)
		if err != nil {
			return nil, err
		}

		columns, err := records.Keys()
		if err != nil {
			fmt.Println(err.Error())
		}
		var results []map[string]interface{}
		for records.Next() {
			item := make(map[string]interface{})
			for i, value := range records.Record().Values {
				switch value.(type) {
				case nil:
					item[columns[i]] = nil
				case []byte:
					item[columns[i]] = string(value.([]byte))
				default:
					item[columns[i]] = value
				}
			}
			results = append(results, item)
		}
		return results, nil
	})
	if err != nil {
		log.Fatal("Error: %s", err.Error())
	}
	return results
}

// https://guides.neo4j.com/sandbox/twitter/index.html
func main() {
	cfg := config{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatal(err)
	}
	level, _ := log.ParseLevel(cfg.LogLevel)
	log.SetLevel(level)

	driver, err := neo4j.NewDriver(cfg.Neo4jUri, neo4j.NoAuth())
	if err != nil {
		log.Fatal(err)
	}
	defer driver.Close()

	session := driver.NewSession(neo4j.SessionConfig{
		AccessMode:   neo4j.AccessModeRead,
		DatabaseName: cfg.Database,
	})
	defer session.Close()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
	})

	http.HandleFunc("/your-mentions", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		query := `MATCH
					  (u:Me:User)-[p:POSTS]->(t:Tweet)-[:MENTIONS]->(m:User)
					WITH
					  u,p,t,m, COUNT(m.screen_name) AS count
					ORDER BY
					  count DESC
					RETURN
					  m.screen_name, COUNT(m.screen_name)
					ORDER BY
						COUNT(m.screen_name) DESC
					LIMIT 10`

		err = json.NewEncoder(w).Encode(getResults(session, query))
		if err != nil {
			log.Println("error response:", err)
		}
	})

	http.HandleFunc("/most-influential-followers", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		query := `MATCH 
					(follower:User)-[:FOLLOWS]->(u:User:Me) 
					RETURN 
					follower.screen_name AS user, follower.followers AS followers 
					ORDER BY 
					followers DESC 
					LIMIT 10`

		err = json.NewEncoder(w).Encode(getResults(session, query))
		if err != nil {
			log.Println("error response:", err)
		}
	})

	http.HandleFunc("/most-tagged", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		query := `MATCH 
					(h:Hashtag)<-[:TAGS]-(t:Tweet)<-[:POSTS]-(u:User:Me) 
					WITH 
					h, COUNT(h) AS Hashtags 
					ORDER BY 
					Hashtags DESC 
					LIMIT 10 
					RETURN 
					h.name, Hashtags`

		err = json.NewEncoder(w).Encode(getResults(session, query))
		if err != nil {
			log.Println("error response:", err)
		}
	})

	http.HandleFunc("/follower-recommendations", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		query := `MATCH
					  (ou:User)-[:POSTS]->(t:Tweet)-[mt:MENTIONS]->(me:User:Me)
					WITH
					  DISTINCT ou, me
					WHERE
					  (ou)-[:FOLLOWS]->(me)
					  AND NOT
						(me)-[:FOLLOWS]->(ou)
					RETURN
					  ou.screen_name
					order by
					  toUpper(ou.screen_name)`

		err = json.NewEncoder(w).Encode(getResults(session, query))
		if err != nil {
			log.Println("error response:", err)
		}
	})

	http.HandleFunc("/common-hashtags", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		query := `MATCH
					  (me:User:Me)-[:POSTS]->(tweet:Tweet)-[:TAGS]->(ht)
					MATCH
					(ht)<-[:TAGS]-(tweet2:Tweet)<-[:POSTS]-(sugg:User)
					WHERE
					  sugg <> me
						AND NOT
  						(tweet2)-[:RETWEETS]->(tweet)
					WITH
					  sugg, collect(distinct(ht)) as tags
					RETURN
					  sugg.screen_name as friend, size(tags) as common
					ORDER BY
					  common DESC
					LIMIT 20`

		err = json.NewEncoder(w).Encode(getResults(session, query))
		if err != nil {
			log.Println("error response:", err)
		}
	})

	log.Fatal(http.ListenAndServe(":80", nil))
}
