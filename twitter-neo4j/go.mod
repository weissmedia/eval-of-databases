module twitter-neo4j

go 1.18

require (
	github.com/caarlos0/env/v6 v6.9.3
	github.com/lib/pq v1.10.6
	github.com/neo4j/neo4j-go-driver/v4 v4.4.3
	github.com/sirupsen/logrus v1.8.1
)

require golang.org/x/sys v0.0.0-20211124211545-fe61309f8881 // indirect
