#!/bin/bash

docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
docker volume prune -f
make run/twitter DARGS="--build -d neo4j"
#docker exec -it eval-of-nosql_neo4j_1 neo4j-admin import --multiline-fields=true --database=twitter --nodes=import/twitter-v2-43-user.csv --relationships=import/twitter-v2-43-follows.csv
docker exec -it eval-of-nosql_neo4j_1 neo4j-admin load --from import/twitter-v2-43.dump --database "twitter"

#sleep 20

docker stop $(docker ps -aq)
make run/twitter CARGS="--env-file .envs/.twitter"
#docker exec -it eval-of-nosql_neo4j_1 cypher-shell 'CALL apoc.export.csv.all("twitter-v2-43.csv", {})'
#docker exec -it eval-of-nosql_neo4j_1 bash 'cat example.cypher | bin/cypher-shell'