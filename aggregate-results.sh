#!/usr/bin/env bash

file=$@
CMDRunnerPath=/home/mathias/Programme/apache-jmeter-5.0/bin

# Aggregate Results
$CMDRunnerPath/JMeterPluginsCMD.sh  --generate-csv "${file%.*}_AggregateReport.csv" --input-jtl  "${file}"  --plugin-type AggregateReport
