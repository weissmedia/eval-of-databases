#!/usr/bin/env bash

# https://programming.vip/docs/jmeterplugincmd-command-line-tool-usage-details.html
# https://www.ubik-ingenierie.com/blog/automatically-generating-nice-graphs-at-end-of-your-load-test-with-apache-jmeter-and-jmeter-plugins/?cn-reloaded=1
# https://jmeter-plugins.org/wiki/JMeterPluginsCMD/
# https://jmetervn.com/2016/12/14/how-to-install-plugins-in-jmeter-3-x-via-command-line/
# https://octoperf.com/blog/2017/10/19/how-to-analyze-jmeter-results/#simple-data-writer

file=$@
#width=800
#height=600
CMDRunnerPath=/home/mathias/Programme/apache-jmeter-5.0/bin

#generate png
plugins=(ResponseTimesDistribution ResponseTimesPercentiles)
for plugin in "${plugins[@]}"; do
  $CMDRunnerPath/JMeterPluginsCMD.sh --tool Reporter --paint-gradient no --limit-rows 20 --generate-png "${file%.*}_${plugin}.png" --input-jtl  "${file}"  --plugin-type "${plugin}"
done