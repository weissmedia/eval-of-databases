package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"github.com/caarlos0/env/v6"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
)

type config struct {
	PostgresUrl string `env:"POSTGRES_URL,required"`
	LogLevel    string `env:"LOG_LEVEL" envDefault:"debug"`
	DataFile    string `env:"DATA_FILE" envDefault:"/data.jsonl"`
}

func main() {
	cfg := config{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatal(err)
	}

	level, _ := log.ParseLevel(cfg.LogLevel)
	log.SetLevel(level)

	db, err := sql.Open("postgres", cfg.PostgresUrl)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	start, end := 0, 0
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()
		if q.Has("s") {
			start, _ = strconv.Atoi(q.Get("s"))
		}
		if q.Has("e") {
			end, _ = strconv.Atoi(q.Get("e"))
		}
		log.Println(start, end)
		w.Header().Set("Content-Type", "application/json")

		dataFile, err := os.Open(cfg.DataFile)
		if err != nil {
			log.Fatal(err)
		}
		defer dataFile.Close()
		scanner := bufio.NewScanner(dataFile)

		i := 0
		for scanner.Scan() {
			i++
			if i < start {
				continue
			}
			query := `INSERT INTO accesslog (data) VALUES($1);`
			result, err := db.Exec(query, scanner.Bytes())
			if err != nil {
				http.Error(w, err.Error(), http.StatusServiceUnavailable)
			}
			ret := false
			if result != nil {
				affected, err := result.RowsAffected()
				if err != nil {
					log.Errorln(err)
				}
				ret = affected == 1
			}
			res := fmt.Sprintf("Inserted document (%v): %v", i, ret)
			log.Println(res)
			fmt.Fprintln(w, res)
			if i >= end {
				break
			}
		}
	})
	log.Fatal(http.ListenAndServe(":80", nil))
}
