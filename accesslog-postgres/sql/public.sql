DROP TABLE IF EXISTS "public"."accesslog";
CREATE TABLE "public"."accesslog" (
  "id" serial PRIMARY KEY,
  "data" jsonb NOT NULL
);