#!/usr/bin/env python
import os
import glob
import shutil
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--target', dest='target', required=True, help='path')
    args = parser.parse_args()
    files = glob.glob('./data/*/reports/*-mp.png', recursive=False)
    for file in files:
        print(file)
        *path_args, filename = file.split('/')[2:]
        path = f"{args.target}/{'/'.join(path_args)}"
        os.makedirs(path, exist_ok=True)
        shutil.copy(file, f"{path}/{filename}")
    print('done')
